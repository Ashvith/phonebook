# Phonebook

A simple CRUD webapp with user management, built with Rails.

## Technologies used:

- Rails
- Devise
- Propshaft
- Turbo
- Stimulus
- JS Bundling
- CSS Bundling
- ActionView
- ActiveRecord
- ActionMailer
- esbuild
- Sass
- Bulma
- Material Symbols
- PostgreSQL

## How to access the webapp
Deploy link: [Click here to access webapp](https://phonebook-nm5b.onrender.com/)

## How to install
1. `git clone` the project locally (creating fork is optional).
2. Ensure that the pre-requisite dependencies are installed (Ruby 3.1.1, Yarn 1.22.19, Node 16.15.0, PostgreSQL 13.9). [`asdf-vm`](https://asdf-vm.com/) is a great way to deal with packages, if in case, they're not available in your system's default package manager.
3. Run `bin/setup` command to prepare the app for execution.
4. Run `bin/dev` to run the Rails app in development mode.